var engine = require('engine.io');
var server =engine.listen(8084);
var srv=require('http').createServer();
//=======
var STREAM_SECRET = process.argv[2],
	STREAM_PORT = process.argv[3] || 8082,
	WEBSOCKET_PORT = process.argv[4] || 8084,
	STREAM_MAGIC_BYTES = 'jsmp'; // Must be 4 bytes

var proc = require("./spawn_module").args;
var OPT_FILE_STREAM=["-re","-i","","-codec","copy","-f","mpeg1video","-b:v","800k","http://127.0.0.1:8082/0626/640/480"];
//=======
server.on('connection', function(socket){
  startStream(srv,socket,"out.mp4");
});

// …
function startStream(httpSrv,socket,streamType){
	var child=null;
	httpSrv.on('request',function(request, response){
	    var params = request.url.substr(1).split('/');
        if(params[0] == STREAM_SECRET) {
		    width = (params[1] || 320)|0;
		    height = (params[2] || 240)|0;
		    
			// Send magic bytes and video size to the newly connected socket
           // struct { char magic[4]; unsigned short width, height;}
	        var streamHeader = new Buffer(8);
	        streamHeader.write(STREAM_MAGIC_BYTES);
	        streamHeader.writeUInt16BE(width, 4);
	        streamHeader.writeUInt16BE(height, 6);
			socket.binaryType='buffer';
			if(socket.readyState=="open") socket.send(streamHeader,function(){console.log("header sent");});
		    console.log(
			'Stream Connected: ' + request.socket.remoteAddress + 
			':' + request.socket.remotePort + ' size: ' + width + 'x' + height
		    );
		    request.on('data', function(data){
			  //socket.send(data,{binary:true});
				  //socket.binaryType='buffer';
			      socket.send(data.toString("base64"));
		    });
	    }
	    else{
		console.log(
			'Failed Stream Connection: '+ request.socket.remoteAddress + 
			request.socket.remotePort + ' - wrong secret.'
		);
		response.end();
	    }
    });
    httpSrv.listen(STREAM_PORT,function(err){
	    if (err) {throw err};
		if(streamType=="live") child = proc.spawn(proc.cmd,proc.options); //lance la capture
		else{ 
		   OPT_FILE_STREAM[2]=streamType; //adjusting the dynamic input file
	       child = proc.spawn(proc.cmd,OPT_FILE_STREAM); //lance la capture
		}
	    child.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
        });
    }6);
	httpSrv.on('close',function(){
		child.kill();
	});
}
