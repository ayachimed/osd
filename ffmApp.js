var STREAM_SECRET      = process.argv[2],
   	STREAM_PORT        = process.argv[3] || 8082,
	   WEBSOCKET_PORT     = process.argv[4] || 8084,
	   STREAM_MAGIC_BYTES = 'jsmp';
	
//=============================
var nots=[];//contient la liste des notifications! 
var nCounter=0;    //compteur de notifications hors ligne
var cntr=0;
//=============================
var dt=0;
var SpiderMan;
var pass;
var i=0;
var watcher0=null,tmpWatcher=null;
var Timer0=null,Timer1=null,Timer2=null,Timer5=null;
//==============================
var temp=require('./temp_mod/temp_module');
var http=require('http');
var srv=http.createServer();

//var io=require('socket.io').listen(srv0);
var fs=require('fs');
var chokidar = require('chokidar');
var proc = require("./spawn_module").args;
//streaming options:
var OPT_FILE_STREAM=["-re","-i","","-codec","copy","-f","mpeg1video","-b:v","800k","http://127.0.0.1:8082/0626/640/480"];
if(process.argv.length < 3){
	console.log(
		'Usage: \n' +
		'node stream-server.js <secret> [<stream-port> <websocket-port>]'
	);
	process.exit();
}
//websocket server :: 
var socketServer = new (require('ws').Server)({port: WEBSOCKET_PORT});



//var opt={persistent:true,recursive:true};
//---MaDame la fonction -hors ligne- (ws version)---------------
function watchAndSoOn(w,conn){
	var superMan="";
	var errIndex=false;
	w.on('add',function(event,filename){
		var mtn=new Date();
		superMan=mtn.toGMTString();
		if(conn=="closed")
		    nots.push(superMan);
		else if(conn=="open")
			   socket.send(superman);
				  //07-02-2016/00:38/je n'en suis pas sûr
		
	 var Timer=setInterval(function(){//pour s'assurer de la creation du directory
			fs.mkdir("./notifications/"+superMan,function(err){
     if(!err){				
			     clearInterval(Timer);
			     fs.readFile("./notifications/not1/out.mp4",function(err,data){
				      fs.writeFile("./"+superMan+"/out.mp4",data,function(err,data){
					       if(err){
						        console.log("error while writing data!");
					         throw err;
					       }
					       else del();
				      });
		      });
				}else console.log("error while making dir :( ... retrying");
		 });
	  },500);
	});
}
//================================
 
//---tfoooooo 3la fonction------------------
function del(){
	var j=13;
	Timer5=setInterval(function(){ //pour forcer la suppression
		fs.unlink("./notifications/not1/VIDEOFFMPEG",function(err){
 	 	  if(err) {console.log("error de suppression ...retrying");}
 	 	  else clearInterval(Timer5);
 	});
		 
	},500);
}
//===========================
//-------fonction save_video-------------
function save_video(){
    var options=["-f","-i","video=MyCamera","./notifications/not1/out.mp4"];
    var child=proc.spawn(proc.cmd,options);
	   child.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
	   //cette partie sera utilisée s'il ya pas d'option pour specifier la durée 
	   //de la video  !
	   setTimeOut(function(){
		    child.kill();
	   },10000);
}
//---------------------------------------
//-------fonction generic-stream---------
function startStream(httpSrv,socket,streamType){
	var child=null;
	httpSrv.on('request',function(request, response){
	    var params = request.url.substr(1).split('/');
     if(params[0] == STREAM_SECRET) {
		    width = (params[1] || 320)|0;
		    height = (params[2] || 240)|0;
			// Send magic bytes and video size to the newly connected socket
           // struct { char magic[4]; unsigned short width, height;}
	     var streamHeader = new Buffer(8);
	      streamHeader.write(STREAM_MAGIC_BYTES);
	      streamHeader.writeUInt16BE(width , 4);
	      streamHeader.writeUInt16BE(height, 6);
			  if(socket.readyState==1) socket.send(streamHeader, {binary:true});
		    console.log(
			    'Stream Connected: ' + request.socket.remoteAddress + 
			    ':' + request.socket.remotePort + ' size: ' + width + 'x' + height
		    );
		    request.on('data', function(data){
			      socket.send(data,{binary:true});
		    });
	    }
	    else{
		    console.log(
			    'Failed Stream Connection: '+ request.socket.remoteAddress + 
			     request.socket.remotePort + ' - wrong secret.'
		    );
		    response.end();
	    }
  });
  httpSrv.listen(STREAM_PORT,function(err){
	    if (err) {throw err};
		   if(streamType=="live") child = proc.spawn(proc.cmd,proc.options); //lance la capture
		   else{ 
		      OPT_FILE_STREAM[2]=streamType; //adjusting the dynamic input file
	       child =proc.spawn(proc.cmd,OPT_FILE_STREAM); //lance la capture
		   }
	    child.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
     });
   });
	 httpSrv.on('close',function(){
		   child.kill();
	 });
}
  //======c juste un test de simulation du PIR==========
 var auxWatcher=chokidar.watch("./PIR/input.txt");
 auxWatcher.on("change",function(event,filename){
		 //tout simplement !
		 save_video();
 });
 //=====================================================

 //  watcher0=chokidar.watch("./notifications/not1"); // /not1/ va contenir le 'flux' des videos enregistré =>c'un 'buffer' 
  
   //watchAndSoOn(watcher0,"closed");   //accrocher le watch et faire le travail quotidien!

//==========En Fin Qqn S'est Connecté==================
socketServer.on('connection', function(socket) {
	console.log( 'New WebSocket Connection ('+socketServer.clients.length+' total)' );
	//=====================================
	socket.on('message',function(data,flags){
	//envoi des notifications enregistrées hors connexion(ws version)
	  if(data.toString('utf8')=="notify_tab"){
	   // watcher0.close(); //arreter le watch hors connexion
	    var ctr=0;
	    Timer5=setInterval(function(){
		     if(ctr<nots.length)
		       socket.send(nots[ctr++]);
	      else clearInterval(Timer5);
	    },1000);
	    //----travail quotidien-------  
	
	    watcher0=chokidar.watch("./notifications/not1");
	    watchAndSoOn(watcher0,"open");
	    //=====================================
	  }
    //----------click sur une notification-----------
	  else if(data.toString('utf8').split('-')[0]=="click"){
		  var fileName=data.toString('utf8').split('-')[1];
		  startStream(srv,socket,fileName);
	  }
	//-----------------------------------------------
	//Reponse à tmp-tab
	 else if(data.toString('utf8')=="tmp_tab"){
		 tmpWatcher=chokidar.watch(__dirname+"/temp_mod/tmpView.txt",{awaitWriteFinish:true});
	 	//on doit arreter le streaming =>stop http server !
	 	srv.close(function(err){  //remember:on srv closing we kill child-process!
	 		 if(err) console.log("server already closed !");
	 	});
	 	temp.tmpData(socket,tmpWatcher);
	 }
	 //Reponse à live-stream-tab
	 if(data.toString()=="str"){
		//tout simplement
		if(tmpWatcher!=null)tmpWatcher.close();
		startStream(srv,socket,"./out.mp4"); 
	 }
    });
	//=====================================
	socket.on('close', function(code, message){
    srv.close();
		  console.log( 'Disconnected WebSocket ('+socketServer.clients.length+' total)' );
		 //watcher0.close();
   //------------lancer le 'demon' (notifications hors connexion)
		  watcher0=chokidar.watch("./notifications/not1");
    watchAndSoOn(watcher0,"closed");
	});
	
});



//fonction qui envoi une image (ws version)
function sendPic(name,socket)
{
	fs.access(name,fs.F_OK,function(err){
	if(!err){
	  fs.readFile("./"+name,function(err,data){
		   if(err)console.log('error while reading file:'+err.message);
		   else{
	        socket.send({buffer:data.toString('base64')},{binary:true});
			      console.log("image envoyée");
		   }
	  });
	}else 
		fs.readFile(__dirname+"/ttok.jpg",function(err,data){
		   if(err)console.log('error while reading file:'+err.message);
		   else{
	       socket.send({buffer:data.toString('base64')},{binary:true});
		   }
	    });
    });
}

//======================================================
//fonction qui envoi les données de tmperature (ws version)
/*
function tmpData(socket){
	var watcher=chokidar.watch("../C#_tp/tmpView.json");
	var daba=new Date(Date.now());
	watcher.on("add",function(event,opt,filename){
		fs.readFile("./tmpView.txt","utf8",function(err,data){
			//var jsData=JSON.parse(data);....masd9ach b JSON lmhm on s'en fou!
			if(!err){
			socket.send({data.toString('utf8'),date:daba.toGMTString()},function(err){
				if(err) {
					console.log("tmp_data_network_err :( !");
				}
			});
		    //console.log("1tmp Val envoyée! n°="+(cntr++)); //it's just a stupid debugging trick ! 
			}
			else {
			 	console.log("error while reading tmp data");   
			}
		});
		
	});
}*/

/*
	socket.on('gclik',function(data){
		SpiderMan=data.
		if(Timer2!=null)clearInterval(Timer2); //stop !
		else                                   //play !
		 Timer2=setInterval(function(){
           sendFile(data+"/img"+count+".png",socket);
           if((++count)==20){
		 	 count=0;
		 	 clearInterval(Timer2);
		   }
		 },100);
	});
	 /////////////////////////////////////////////////////////
	
	socket.on("mot3",function(data){
		//console.log("client deconnecté!!!!");
		if(Timer0!=null)clearInterval(Timer0);
		if(Timer1!=null)clearInterval(Timer1);
	});
*/
